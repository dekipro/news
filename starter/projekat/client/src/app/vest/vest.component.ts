import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient,HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-vest',
  templateUrl: './vest.component.html',
  styleUrls: ['./vest.component.css']
})
export class VestComponent implements OnInit {


  vest: any={
    naslov: '',
    opis: '',
    sadrzaj: ''
  };

  komentar: any;

  constructor(private route: ActivatedRoute, private http: HttpClient) {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.http.get(`api/vesti/${id}`).subscribe(response => {
        this.vest = response as any;
      });
    });
   }
   
  ngOnInit() {
     
  }

  loadData(){
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.http.get(`api/vesti/${id}`).subscribe(response => {
        this.vest = response as any;
      });
    });
  }
   
  comment(){
    const id = this.vest.id;
    console.log(this.vest.id);
    const params = new HttpParams()
    .set('tekst', this.komentar);
   
    this.http.put(`api/vesti/${this.vest.id}/add-comment`, JSON.stringify(this.vest),{ params })
    .subscribe((data) =>console.log());
   this.loadData();

  }

}
