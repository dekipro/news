import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthenticationService } from 'app/security/authentication.service';

@Component({
  selector: 'app-edit-vest',
  templateUrl: './edit-vest.component.html',
  styleUrls: ['./edit-vest.component.css']
})
export class EditVestComponent implements OnInit {

  vesti: any[];

  vest: any = {
    naslov: '',
    sadrzaj: '',
    opis: ''
  };

  kategorija:any = {
    ime:''
  }

  kategorije: any[];

  currentPage = 0;

  numberOfPages: number;

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.loadData();
    this.loadKategorije();
  }

  loadData() {
    const params = new HttpParams()
      .set('page', this.currentPage.toString())
      .set('size', '5');
    this.http.get('api/vesti', { params }).subscribe( data => {
    // this.http.get(`api/computer-parts?page=${this.currentPage}&size=${5}`).subscribe( data => {
      this.vesti = data['content'] as any[];
      this.numberOfPages = data['totalPages'];
      this.reset();
    });
  }

  loadKategorije() {
    this.http.get('api/kategorije').subscribe( data => {
      this.kategorije = data as any[];
    });
  }

  hasRole(role: string): boolean {
    return this.authenticationService.hasRole(role);
  }

  filter(){      
    const id = this.kategorija.id;
    const params = new HttpParams()
  .set('naslov', this.vest.naslov)
  .set('kategorijaId', id.toString());
  this.http.get('api/vesti', {params}).subscribe( data => {
    this.vesti = data as any[];

  })
}

save() {
  const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  if (this.vest.id === undefined) {
    this.http.post('api/vesti', JSON.stringify(this.vest), {headers}).subscribe((data: any) => {
      this.loadData();
    });
  } else {
    this.http.put(`api/vesti/${this.vest.id}`, JSON.stringify(this.vest), {headers}).subscribe((data: any) => {
      this.loadData();
    });
  }
}

delete(v: any) {
  this.http.delete(`api/vesti/${v.id}`).subscribe(data =>{
    console.log(data);
    this.loadData();
  });
}

edit(n: any) {
  this.vest = n;
}


reset() {
  this.vest = {
    naslov: '',
    sadrzaj: '',
    opis: ''
  };
}

byId(item1: any, item2: any) {
  if ( !item1 || !item2 ) {
    return false;
  }
  return item1.id === item2.id;
}

changePage(x: number) {
  if (this.currentPage + x >= 0 && this.currentPage + x < this.numberOfPages) {
    this.currentPage += x;
    this.loadData();
  }
}

}
