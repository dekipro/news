import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVestComponent } from './edit-vest.component';

describe('EditVestComponent', () => {
  let component: EditVestComponent;
  let fixture: ComponentFixture<EditVestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
