import { Component, OnInit } from '@angular/core';
import { Response, RequestOptions,
         Headers } from '@angular/http';

import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthenticationService } from 'app/security/authentication.service';

@Component({
  selector: 'app-main-component',
  templateUrl: './main-component.component.html',
  styleUrls: ['./main-component.component.css']
})
export class MainComponentComponent implements OnInit {

  vesti: any[];

  vest: any = {
    naslov: '',
    sadrzaj: '',
    opis: ''
  };

  kategorija:any = {
    ime:''
  }

  kategorije: any[];

  currentPage = 0;

  numberOfPages: number;


  ngOnInit(): void {
    this.loadData();
    this.loadKategorije();
  }

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
  }

  loadData() {
    const params = new HttpParams()
      .set('page', this.currentPage.toString())
      .set('size', '5');
    this.http.get('api/vesti', { params }).subscribe( data => {
    // this.http.get(`api/computer-parts?page=${this.currentPage}&size=${5}`).subscribe( data => {
      this.vesti = data['content'] as any[];
      this.numberOfPages = data['totalPages'];
      this.reset();
    });
  }

  loadKategorije() {
    this.http.get('api/kategorije').subscribe( data => {
      this.kategorije = data as any[];
    });
  }

  hasRole(role: string): boolean {
    return this.authenticationService.hasRole(role);
  }

    filter(){      
      const id = this.kategorija.id;
      const params = new HttpParams()
    .set('naslov', this.vest.naslov)
    .set('kategorijaId', id.toString());
    this.http.get('api/vesti', {params}).subscribe( data => {
      this.vesti = data as any[];

    })
  }


  reset() {
    this.vest = {
      naslov: '',
      sadrzaj: '',
      opis: ''
    };
  }


  changePage(x: number) {
    if (this.currentPage + x >= 0 && this.currentPage + x < this.numberOfPages) {
      this.currentPage += x;
      this.loadData();
    }
  }
}
