import { Component, OnInit, Input } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-child-comment',
  templateUrl: './child-comment.component.html',
  styleUrls: ['./child-comment.component.css']
})
export class ChildCommentComponent implements OnInit {

  @Input()
  komentar: any;

  komentarNaKomentar:any;
  
  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
  }

  comment(){
    console.log(this.komentarNaKomentar);
    const id = this.komentar.id;
    const params = new HttpParams()
    .set('tekst', this.komentarNaKomentar);
   
    this.http.put(`/api/komentari/${this.komentar.id}/add-comment`, JSON.stringify(this.komentar),{ params })
    .subscribe((data) =>console.log());
  }
}
