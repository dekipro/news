package vp.spring.rcs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Komentar;
import vp.spring.rcs.model.Vest;
import vp.spring.rcs.service.KomentarService;
import vp.spring.rcs.service.VestService;
import vp.spring.rcs.web.dto.CommonResponseDTO;



@RestController
@RequestMapping(value="/api/vesti")
public class VestController {
  
	@Autowired
	VestService vestService;
	
	@Autowired
	KomentarService komentarService;
	
	
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity<Page<Vest>> findAll(Pageable pageable){
		Page<Vest> vesti = vestService.findAll(pageable);
		return new ResponseEntity<>(vesti, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Vest> findOne(@PathVariable Long id){
		Vest vest = vestService.findOne(id);
		
		return new ResponseEntity<>(vest, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, params= {"naslov", "kategorijaId"})
	public ResponseEntity<List<Vest>> findByNaslovContainsAndKategorijaId(@RequestParam String naslov, @RequestParam Long kategorijaId){
		List<Vest> vesti = vestService.findByNaslovContainsAndKategorijaId(naslov, kategorijaId);
		
		if(vesti != null) {
			return new ResponseEntity<>(vesti, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<Vest> save(@RequestBody Vest vest){
		Vest ves = vestService.save(vest);
		return new ResponseEntity<>(ves, HttpStatus.OK);
	}
	
	@RequestMapping(value = "{id}/add-comment", method = RequestMethod.PUT, params="tekst")
	public ResponseEntity<Komentar> addComment(@PathVariable Long id,@RequestParam String tekst) {
		Vest vest = vestService.findOne(id);
		Komentar komentar = new Komentar();
		komentar.setTekst(tekst);
		komentar = komentarService.save(komentar);
		vest.getKomentari().add(komentar);
		 vest = vestService.save(vest);
		return new ResponseEntity<>(komentar, HttpStatus.CREATED);
	}
	


	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	ResponseEntity<Vest> update(@PathVariable Long id, @RequestBody Vest vest){
		Vest ves = vestService.findOne(id);
		ves.setNaslov(vest.getNaslov());
		ves.setOpis(vest.getOpis());
		ves.setSadrzaj(vest.getSadrzaj());
		ves.setKategorija(vest.getKategorija());
		
		ves = vestService.save(ves);
		return new ResponseEntity<>(ves , HttpStatus.OK);
	}
	
	

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	ResponseEntity<CommonResponseDTO> update(@PathVariable Long id){
		vestService.remove(id);
		return new ResponseEntity<>(new CommonResponseDTO("deleted"), HttpStatus.OK);
	}
	
}
