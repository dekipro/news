package vp.spring.rcs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vp.spring.rcs.data.KomentarRepository;
import vp.spring.rcs.model.Komentar;
  
 
@Service
public class KomentarService {

	@Autowired
	KomentarRepository repo;
	
	public Komentar findOne(Long id) {
		return repo.findOne(id);
	}
	
	public Komentar save(Komentar kom) {
		return repo.save(kom);
	}
}
