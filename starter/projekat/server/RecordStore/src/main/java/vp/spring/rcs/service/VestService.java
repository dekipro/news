package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vp.spring.rcs.data.VestRepository;
import vp.spring.rcs.model.Vest;

 
@Service
public class VestService {

	@Autowired
	private VestRepository vestRepository;
	
	public Vest findOne(Long id) {
		return vestRepository.findOne(id);
	}

	public List<Vest> findAll() {
		return vestRepository.findAll();
	}
	
	public Page<Vest> findAll(Pageable page) {
		return vestRepository.findAll(page);
	}

	public Vest save(Vest vest) {
		return vestRepository.save(vest);
	}

	public void remove(Long id) {
		vestRepository.delete(id);
	}
	
	public List<Vest>findByNaslovContainsAndKategorijaId(String naslov, Long kategorijaId){
		return vestRepository.findByNaslovContainsAndKategorijaId(naslov, kategorijaId);
	}
}
