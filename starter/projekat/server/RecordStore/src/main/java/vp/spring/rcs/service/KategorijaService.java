package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vp.spring.rcs.data.KategorijaRepository;
import vp.spring.rcs.model.Kategorija;

 
@Service
public class KategorijaService {
	@Autowired
	KategorijaRepository repo;
	
	public List<Kategorija>findAll(){
		return repo.findAll();
	}
}
