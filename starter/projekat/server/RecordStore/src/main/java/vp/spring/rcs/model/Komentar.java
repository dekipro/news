package vp.spring.rcs.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

 
@Entity
public class Komentar {

	@Id
	@GeneratedValue
	private Long id;
	
	private String tekst;
	
	@OneToMany()
	private List<Komentar> children;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTekst() {
		return tekst;
	}

	public void setTekst(String tekst) {
		this.tekst = tekst;
	}

	public List<Komentar> getChildren() {
		return children;
	}

	public void setChildren(List<Komentar> children) {
		this.children = children;
	}

	public Komentar() {
		super();
	}
	
	
}
