package vp.spring.rcs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Komentar;
import vp.spring.rcs.model.Vest;
import vp.spring.rcs.service.KomentarService;

@RestController
@RequestMapping(value="/api/komentari")
public class CommentController {

	@Autowired
	KomentarService komentarService;
	
	@RequestMapping(value = "{id}/add-comment", method = RequestMethod.PUT, params="tekst")
	public ResponseEntity<Komentar> addComment(@PathVariable Long id,@RequestParam String tekst) {
		Komentar komentar = komentarService.findOne(id);
		Komentar komentarZaDodati = new Komentar();
		komentarZaDodati.setTekst(tekst);
		komentarZaDodati = komentarService.save(komentarZaDodati);
		komentar.getChildren().add(komentarZaDodati);
		komentar = komentarService.save(komentar);
		return new ResponseEntity<>(komentar, HttpStatus.CREATED);
	}
}
