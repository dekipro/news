package vp.spring.rcs.data;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vp.spring.rcs.model.Vest;

 
@Repository
public interface VestRepository extends JpaRepository<Vest, Long>{
	
	public Page<Vest> findAll(Pageable pageable);
	public List<Vest> findByNaslovContainsAndKategorijaId(String name, Long kategorijaId);

}
